﻿using MiPrimeritoEFProject;

using (var context = new DistilleryContext())
{
    //delete db if exists 
    context.Database.EnsureDeleted();
    //creates db if not exists 
    context.Database.EnsureCreated();

    //generem un objecte de tipus Alcohol. Fem servir els accessors [{ get; set; }]
    Alcohol ginebra = new Alcohol()
    {
        AlcoholName = "Puerto de Indias",
        AlcoholDescription = "Es un alcohol que bueno que tal",
        AlcoholGradation = 40.05
    };

    //Generem un objecte mascota
    Mascota m = new Mascota();
    m.MascotaName = "El Indio";
    m.MascotaDescription = "Bueno está claro no???";

    //Afegim la relació entre mascota i alcohol. Podem afegir tant a l'Alcohol com a la Mascota (trieu un...)
    //m.Alcohol = ginebra; 
    ginebra.Mascota = m;

    //Afegim els regfistres als sets del context i fem un SaveChanges.
    context.Mascotas.Add(m);
    context.Alcohols.Add(ginebra);
    context.SaveChanges();
/*

    //Per obtenir tots els registres d'una taula podem accedir directamen al set respectiu del context (p.e.: context.Alcohols)
    foreach(Alcohol a in context.Alcohols)
    {
        if(a.AlcoholId==2)
            context.Alcohols.Remove(a); //Esborrem una entitat
        if(a.AlcoholId==1)
        {
            a.AlcoholName = "Esto ha ido modificado";
            context.Alcohols.Update(a); //Fem update d'una entitat
        }
            
    }*/

   // context.SaveChanges(); //Sempre hem de guardar els canvis !!!
}
