﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeritoEFProject
{
    public class Alcohol
    {
        //Generem els atributs de la nostra Entitat. Si fem servir la convenció d'EF, el programa ens detecta automàticament les columnes.[ClasseAtribut]
        public int AlcoholId { get; set; }
        public string AlcoholName { get; set; }
        public string AlcoholDescription { get; set; }
        public double AlcoholGradation { get; set; } = 0.0;    

        //OneToOne
        //Referencia a la PK de l'altra entitat. És a dir: FOREIGN KEY
        public int MascotaId { get; set; }
        //Referencia a l'altra entitat de la relació.
        public Mascota Mascota { get; set; }


    }
}
