﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeritoEFProject
{
    //Aquesta classe ens servirà de DAO, és a dir, s'encarregará de gestionar totes les operacions de la BBDD
    public class DistilleryContext : DbContext
    {
        //Indiquem les entitats amb les que treballarem en la BBDD utilitzant el DbSet de DbContext
        public DbSet<Alcohol> Alcohols { get; set;}
        public DbSet<Mascota> Mascotas { get; set; }

        //Amb el OnConfiguring i el UseSqlServer preparem la conexió amb la BBDD
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //String de conexió: "Server=[ruta del server];Database=[nom de la BBDD];Trusted_Connection=True;"
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=DistilleryDB;Trusted_Connection=True;");
        }
    }
}
