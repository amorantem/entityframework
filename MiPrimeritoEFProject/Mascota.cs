﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeritoEFProject
{
    public class Mascota
    {
        public int MascotaId { get; set; }
        public string MascotaName { get; set; }
        public string MascotaDescription { get; set; }

        //Relacio OneToOne.
        //Tenim una referencia a l'entitat amb la qual es relaciona. Com que la columna la volem a l'altra entitat, no li fem referencia de la id.
        //public int AlcogolId { get; set; } <---- NOMÉS SI VOLEM LA COLUMNA EN LA TAULA DE MASCOTA.
        public Alcohol Alcohol { get; set; }

    }
}
