﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeritoEFProject
{
    public class Marca
    {
        public int MarcaId { get; set; }
        public string MarcaName { get; set; } = string.Empty;
        public string MarcaDescription { get; set;} = string.Empty;
    }
}
